﻿using Discovery.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discovery.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            DiscoveryService service = new DiscoveryService();
            foreach (var st in service.GetAllCustomers())
            {
                Console.WriteLine(st.FirstName);
            }
        }
    }
}
