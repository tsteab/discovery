﻿using Discovery.Data;
using Discovery.Data.Entites;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discovery.CustomerManagementUI
{
    public partial class AddStudentForm : Form
    {
        DiscoveryService service;
        Customer currentCustomer;
        StudentForm studentForm;


        public AddStudentForm(Customer cust, StudentForm studentForm)
        {
            InitializeComponent();
            service = DiscoveryService.GetDiscoveryService();
            currentCustomer = cust;
            this.studentForm = studentForm;
            
            comboBox1.DataSource = service.GetAllClassRoom();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtFirstName.Clear();
            txtLastName.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Student student = new Student();
            bool finished = false;
            try
            {
                student.FirstName = txtFirstName.Text;
                student.LastName = txtLastName.Text;
                student.Address = currentCustomer.Address;
                student.Room = (ClassRoom)comboBox1.SelectedItem;
                student.Parent = currentCustomer;
                currentCustomer.Children.Add(student);
                service.AddStudent(student);
                service.SaveChanges();
                finished = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (finished)
            {
                studentForm.UpdateList();
                Close();
            }
        }
    }
}
