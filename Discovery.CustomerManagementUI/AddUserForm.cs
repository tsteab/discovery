﻿using Discovery.Data;
using Discovery.Data.Entites;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discovery.CustomerManagementUI
{
    public partial class AddUserForm : Form
    {
        DiscoveryService service;
        Form1 mainForm;
        public AddUserForm(Form1 mainForm)
        {
            InitializeComponent();
            service = DiscoveryService.GetDiscoveryService();
            this.mainForm = mainForm;
        }

        private void btnAddCust_Click(object sender, EventArgs e)
        {
            Customer cust = new Customer();
            bool finished = false;
            try
            {
                cust.Balance = decimal.Parse(txtDebit.Text);
                cust.FirstName = txtFirstName.Text;
                cust.LastName = txtLastName.Text;
                cust.Email = txtEmail.Text;
                cust.Phone = txtPhone.Text;
                cust.Address = txtAddress.Text;
                service.AddCustomer(cust);
                service.SaveChanges();
                finished = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (finished)
            {
                mainForm.UpdateGridView();
                Close();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            foreach (var control in this.Controls.OfType<TextBox>())
            {
                control.Clear();
            }
        }
    }
}
