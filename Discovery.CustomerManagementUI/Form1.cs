﻿using Discovery.Data;
using Discovery.Data.Entites;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discovery.CustomerManagementUI
{
    public partial class Form1 : Form
    {
        DiscoveryService service;
        DiscoveryDbContext context = new DiscoveryDbContext();
        public Form1()
        {
            InitializeComponent();
            service = DiscoveryService.GetDiscoveryService();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            UpdateGridView();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            service.SaveChanges();
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddUserForm userForm = new AddUserForm(this);
            userForm.Show();
        }

        public void UpdateGridView()
        {
            BindingSource bi = new BindingSource();
            bi.DataSource = service.GetAllCustomers();
            dataGridView1.DataSource = bi;
            bindingNavigator1.BindingSource = bi;
            dataGridView1.Refresh();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            int id = (int)dataGridView1.CurrentRow.Cells[0].Value;
            int selectedIndex = dataGridView1.CurrentRow.Index;
            dataGridView1.Rows.RemoveAt(selectedIndex);
            service.Delete(id);
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int)dataGridView1.CurrentRow.Cells[0].Value;
            Customer cs = service.GetCustomer(id);
            StudentForm userForm = new StudentForm(cs);
            userForm.Show();
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == "") dataGridView1.DataSource = context.Customers.ToList();
            else
                dataGridView1.DataSource = context.Customers
                    .Where(x => x.LastName == txtSearch.Text)
                    .OrderBy(x => x.FirstName)
                    .ToList();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = context.Customers.ToList();
            txtSearch.Clear();
        }
    }
}
