﻿using Discovery.Data;
using Discovery.Data.Entites;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discovery.CustomerManagementUI
{
    public partial class StudentForm : Form
    {
        Customer currentCustomer;
        DiscoveryService service;
        
        
        public StudentForm(Customer cs)
        {
            InitializeComponent();
            currentCustomer = cs;
            service = DiscoveryService.GetDiscoveryService();
            lstStudents.DataSource = new BindingList<Student>(cs.Children);
        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            AddStudentForm StudentForm = new AddStudentForm(currentCustomer, this);
            StudentForm.Show();
        }

        private void listClassRoom_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void listClassRoom_MouseDoubleClick(object sender, MouseEventArgs e)
        {
        }

        private void lstStudents_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                Student st = service.GetStudent(((Student)lstStudents.SelectedItem).Id);
                lblClassroom.Text = st.Room.ToString();
            }
            catch (NullReferenceException)
            {
                // does nothing
            }
            
        }

        public void UpdateList()
        {
            currentCustomer = service.GetCustomer(currentCustomer.Id);
            lstStudents.DataSource = new BindingList<Student>(currentCustomer.Children);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int id = ((Student)lstStudents.SelectedItem).Id;
            service.DeleteStudent(id);
            service.SaveChanges();
            UpdateList();
        }
    }
}
