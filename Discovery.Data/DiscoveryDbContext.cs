﻿using Discovery.Data.Entites;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discovery.Data
{
    public class DiscoveryDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<ClassRoom> ClassRooms { get; set; }
        public DiscoveryDbContext()
            :base("data source=(LocalDB)\\MSSQLLocalDB;attachdbfilename=|DataDirectory|\\Discovery.mdf;integrated security=True;connect timeout=30;MultipleActiveResultSets=True;App=EntityFramework")
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
