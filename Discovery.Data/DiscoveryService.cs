﻿using Discovery.Data.Entites;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discovery.Data
{
    public class DiscoveryService
    {
        DiscoveryDbContext context;
        static DiscoveryService service;
        public DiscoveryService()
        {
            // makes the entity Framework.Sql server dll go into the bin
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            context = new DiscoveryDbContext();
        }

        public static DiscoveryService GetDiscoveryService()
        {
            if (service == null)
                service = new DiscoveryService();
            return service;
        }

        public List<Student> GetAllStudents()
        {
            return context.Students.ToList();
        }

        public List<Customer> GetAllCustomers()
        {
            return context.Customers.ToList();
        }

        public List<ClassRoom> GetAllClassRoom()
        {
            return context.ClassRooms.ToList();
        }

        public void AddCustomer(Customer cust)
        {
            context.Customers.Add(cust);
        }

        public void AddStudent(Student student)
        {
            context.Students.Add(student);
        }

        public Student GetStudent(int sid)
        {
            return context.Students.Include("Room").Include("Parent").SingleOrDefault(c => c.Id == sid);
        }

        public Customer GetCustomer(int id)
        {
            return context.Customers.Include("Children").SingleOrDefault(c => c.Id == id);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Customers.Remove(context.Customers.Find(id));
            
        }
        public void DeleteStudent(int id)
        {
            context.Students.Remove(context.Students.Find(id));
        }
    }
}
