﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discovery.Data.Entites
{
    public class ClassRoom
    {
        public ClassRoom()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set;}

        public override string ToString()
        {
            return $"{Name} , {Start.Hours % 13}:{(Start.Minutes == 0 ? "00" : Start.Minutes.ToString())} {(Start.Hours < 12 ? "a.m." : "p.m.")} - {End.Hours % 12}:{(End.Minutes == 0 ? "00" : End.Minutes.ToString())} {(End.Hours < 12 ? "a.m" : "p.m ")}";
        }
    }
}
