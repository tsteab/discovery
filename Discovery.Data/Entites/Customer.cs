﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Discovery.Data.Entites
{
    public class Customer : Person
    {
        private string email;
        private string phone;
        public int Id { get; set; }
        public List<Student> Children { get; set; }
        public decimal Balance { get; set; }
        public Customer()
        {

        }
        public Customer(string firstName, string lastName, string address, string email,
            string phone, List<Student> children) : base(firstName, lastName, address)
        {
            Email = email;
            Phone = phone;
            Children = children;
            Balance = 0.0M;
        }
        
        // setting the email value
        public string Email
        {
            get { return email; }
            set
            {
                try
                {
                    // Regular expression to make sure that a valid email address is entered
                    var regEx = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
+ "@"
+ @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
                    Match m = regEx.Match(value);
                    if (m.Success)
                    {
                        email = value;
                    }
                    else
                    {
                        throw new Exception("Please enter in a valid email address.");
                    }
                }
                catch (Exception)
                {

                    throw new Exception("Please enter in a valid email address");
                }
            }
        }
        // setting the phone number value
        public string Phone
        {
            get { return phone; }
            set
            {
                if (value.Length == 10 || value.Length == 7)
                {
                    phone = value;
                }
                else
                {
                    throw new Exception("Phone number is not valid please try again");
                }
            }
        }





    }
}
