﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Discovery.Data.Entites
{
    public abstract class Person
    {
        public string firstName;
        public string lastName;
        public string address;
        List<Student> children;
        public Person()
        {

        }

        public Person(string firstName, string lastName, string address)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;

        }

        public string FirstName
        {
            get { return firstName; }
            set
            {
                try
                {
                    if (value == "")
                    {
                        throw new Exception("Please enter in a first name");
                    }
                    firstName = value;
                }
                catch (Exception)
                {

                    throw new Exception("Please only enter in vaild letters");
                }
            }
        }
        public string LastName
        {
            get { return lastName; }
            set
            {
                try
                {
                    if (value == "")
                    {
                        throw new Exception("Please enter in a last name");
                    }
                    lastName = value;
                }
                catch (Exception)
                {

                    throw new Exception("Please only enter in vaild letters");
                }
            }
        }
        public string Address { get { return address; }
            set {
                try
                {
                    // Regular expression to make sure that a valid home address is entered
                    var regEx = new Regex(@"^[A-Za-z0-9]+(?:\s[A-Za-z0-9'_-]+)+$");
                    Match m = regEx.Match(value);
                    if (m.Success)
                    {
                        address = value;
                    }
                }
                catch (Exception)
                {

                    throw new Exception("Please enter in a valid address");
                };
            }
        }
    }
}
