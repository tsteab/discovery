﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discovery.Data.Entites
{
    public class Student : Person
    {
        public Student()
            :base()
        {

        }
        public ClassRoom Room { get; set; }
        public Customer Parent { get; set; }
        public int Id { get; set; }
        public Student(string firstName, string lastName, string address, ClassRoom room, Customer parent)
            : base(firstName, lastName, address)
        {
            Parent = parent;
            Room = room;
        }

        public override string ToString()
        {
            return $"{firstName} {lastName}";
        }
    }
}
